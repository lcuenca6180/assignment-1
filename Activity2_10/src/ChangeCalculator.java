import java.util.Scanner;


public class ChangeCalculator {		// #10 on Page 131
		public static void main(String[] args) {   
			Scanner keyboard = new Scanner (System.in);
			System.out.println("Enter price of item");
			System.out.println("(from 25 cents to a dollar, in 5-cent increments):");
			int num1 = keyboard.nextInt();
			int change= 100 - num1;
			int q1=change/25;
			change=change%25;
			int d1=change/10; 
			change=change%10;
			int nick1=change/5;
			change=change%5;
			
			System.out.println("You bought an item for " +num1+ " cents and gave me a dollar, ");
			System.out.println("so your change is:");
			System.out.println(q1 + " quarters, ");
			System.out.println(d1 + " dimes, and ");
			System.out.println(nick1 + " nickel");
			}
}